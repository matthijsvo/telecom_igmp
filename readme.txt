TESTING
=======

1) All elements in /src/elements need to be placed in the /elements/local folder 
   of your click installation (or in a subfolder).
2) To test the protocol, run the script ipnetwork.click found in /src/scripts on
   a port of choice with the argument -p <port>
3) To interact you can either
	- Manually start a telnet console and issue commands
		JOINING: write <clientname>/groupMember.join <multicastgroup>
		LEAVING: write <clientname>/groupMember.leave <multicastgroup>
	  A few samples to copy and paste are provided in the file 
		'handler calls.txt'
	- (Edit and) run the provided telnet test script with the previously chosen
	  port, telnettest.sh <port>

MISC.
=====

- The element mcastetherencap was copied directly from Click's github repository
  since it was not available in the default distribution.

- The address where the multicast server sends its packets was changed to a real
  multicast address 224.0.2.2, insted of the original one which pointed at one 
  of the clients.
