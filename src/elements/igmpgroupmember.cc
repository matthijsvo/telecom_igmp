#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include "igmpgroupmember.hh"
CLICK_DECLS

IGMPGroupMember::IGMPGroupMember() : m_generalQueryTimer(&IGMPGroupMember::handleGeneralQueryTimer, NULL),
                                    m_robustnessVariable(2),
                                    m_unsolicitedReportInterval(1){

}

int IGMPGroupMember::configure(Vector<String> &conf, ErrorHandler *errh) {
        in_addr* addr = new in_addr();
        bool hasR = false;
        uint32_t newR;
        bool hasU = false;
        uint32_t newU;

        if(cp_va_kparse(conf, this, errh,
            "ADDRESS",      cpkM + cpkP,    cpIPAddress,        addr,
            "ROBUSTNESS",   cpkC, &hasR,    cpUnsigned,         &newR,
            "UNSOLICITED",  cpkC, &hasU,    cpUnsigned,         &newU,
            cpEnd) < 0) return -1;

        m_address = *addr;
        delete addr;
        if(hasR) m_robustnessVariable = newR;
        if(hasU) m_unsolicitedReportInterval = newU;

        // init general timer
        m_generalQueryTimer.initialize(this);
        return 0;
}

void IGMPGroupMember::add_handlers(){
    add_write_handler("join", joinGroupHandler, (void *) 0);
    add_write_handler("leave", leaveGroupHandler, (void *) 0);
}

void IGMPGroupMember::push(int port, Packet* p) {
    click_ip* ipHeader = const_cast<click_ip*>(p->ip_header());
    IPAddress dst = IPAddress(ipHeader->ip_dst);

    // If IGMP message:
    //      Parse and delete message
    if(ipHeader->ip_p == 2) {
        parsePacket(p);
        p->kill();
    }

    // If sent to a multicast group this host is a member of
    // or to the all-systems multicast address, 224.0.0.1:
    //      Pass to port 1
    // Else:
    //      Delete message
    else if(dst.is_multicast()){
        HashTable<IPAddress, FilterMode>::iterator it;
        it = m_groupStates.find(dst);
        if((it != m_groupStates.end() && it->second == EXCLUDE) || (dst == IPAddress("224.0.0.1"))) {
            click_chatter("(HOST:%s)\tReceived packet from: %s", IPAddress(m_address).unparse().c_str(), dst.unparse().c_str());
            output(1).push(p);
        } else {
            p->kill();
        }
    }
}

void IGMPGroupMember::handleMembershipQuery(MembershipQuery mq) {
    if(m_groupStates.empty()) {
        click_chatter("(HOST:%s)\tHost received Query but has no state, no Report sent.", IPAddress(m_address).unparse().c_str());
        return;
    }
    uint delay = randomTime(mq.getMaxRespCode());
    // delay is written as amount of tenths of seconds
    // make_msec expects parameters (seconds, milliseconds)
    Timestamp delayTime = Timestamp::make_msec(delay/10, (delay%10)*100);
    Timestamp queryTime = Timestamp::now();
    queryTime += delayTime;

    if(mq.isGeneralQuery()) {
        // If no message is scheduled: schedule one now
        // If a message is scheduled, but later than the proposed delay: schedule with the proposed delay instead
        // Else: do nothing
        if(!m_generalQueryTimer.scheduled() || (m_generalQueryTimer.scheduled() && queryTime < m_generalQueryTimer.expiry())) {
            click_chatter("*HOST:%s)\tScheduled response to General Query after %d ms.", IPAddress(m_address).unparse().c_str(), delay*100);
            m_generalQueryTimer.schedule_at(queryTime);
        }
    }

    else if(mq.isGroupSpecificQuery()) {
        // If no message is scheduled: schedule one now
        // If pending response: schedule one at earliest time, being either that of the pending response or the proposed delay
        HashTable<IPAddress, Timer*>::iterator it;
        it = m_groupSpecificTimers.find(mq.getGroupAddress());

        // If specific group was never encountered before, new Timer needs to be created
        if(it == m_groupSpecificTimers.end()) {
            IPAddress* addr = new IPAddress(mq.getGroupAddress());
            Timer* gsqTimer = new Timer(&IGMPGroupMember::handleGroupSpecificTimer, (void*) addr);
            gsqTimer->initialize(this);
            m_groupSpecificTimers.set(mq.getGroupAddress(), gsqTimer);
            gsqTimer->schedule_at(queryTime);
            click_chatter("*HOST:%s)\tScheduled response to Group-Specific Query after %d ms.", IPAddress(m_address).unparse().c_str(), delay*100);
        } else if(!it->second->scheduled() || (it->second->scheduled() && queryTime < it->second->expiry())) {
            click_chatter("*HOST:%s)\tScheduled response to Group-Specific Query after %d ms.", IPAddress(m_address).unparse().c_str(), delay*100);
            it->second->schedule_at(queryTime);
        }
    }

    else {
        click_chatter("(HOST:%s)\tMembership Query type not supported.", IPAddress(m_address).unparse().c_str());
    }
}

void IGMPGroupMember::handleMembershipReport(MembershipReport mr) {
    // click_chatter("(HOST:%s)\tUnexpectedly received a Membership Report message:", IPAddress(m_address).unparse().c_str());
    // mr.debugPrint();
}

void IGMPGroupMember::handleMulticastPacket(Packet* p){
    click_chatter("(HOST:%s)\treceived multicast packet", IPAddress(m_address).unparse().c_str());
}


void IGMPGroupMember::sendGroupReport(IPAddress addr) {
    HashTable<IPAddress, FilterMode>::iterator it;
    it = m_groupStates.find(addr);

    if(it != m_groupStates.end()) {
        GroupRecord gr(((it->second == INCLUDE) ? GroupRecord::MODE_IS_INCLUDE : GroupRecord::MODE_IS_EXCLUDE), it->first);
        MembershipReport mr;
        mr.addGroupRecord(gr);
        output(0).push(createIGMPPacket(IPAddress("244.0.0.22"), &mr));
    }
}

void IGMPGroupMember::sendAllGroupsReport() {
    for(HashTable<IPAddress, FilterMode>::iterator it = m_groupStates.begin(); it != m_groupStates.end(); ++it) {
        GroupRecord gr(((it->second == INCLUDE) ? GroupRecord::MODE_IS_INCLUDE : GroupRecord::MODE_IS_EXCLUDE), it->first);
        MembershipReport mr;
        mr.addGroupRecord(gr);
        output(0).push(createIGMPPacket(IPAddress("244.0.0.22"), &mr));
    }
}

void IGMPGroupMember::joinGroup(IPAddress multicastGroup) {
    GroupRecord gr(GroupRecord::CHANGE_TO_EXCLUDE_MODE, multicastGroup);
    MembershipReport mr;
    mr.addGroupRecord(gr);

    HashTable<IPAddress, FilterMode>::iterator it;
    it = m_groupStates.find(multicastGroup);
    if(it != m_groupStates.end()) {
        it->second = EXCLUDE;
    } else {
        m_groupStates.set(multicastGroup, EXCLUDE);
    }
    scheduleReport(multicastGroup, mr);
}

void IGMPGroupMember::leaveGroup(IPAddress multicastGroup) {
    HashTable<IPAddress, FilterMode>::iterator it;
    it = m_groupStates.find(multicastGroup);
    if(it != m_groupStates.end()) {
        it->second = INCLUDE;
        GroupRecord gr(GroupRecord::CHANGE_TO_INCLUDE_MODE, multicastGroup);
        MembershipReport mr;
        mr.addGroupRecord(gr);
        scheduleReport(multicastGroup, mr);
    } else {
        click_chatter("(HOST:%s)\tTrying to leave unknown group %s!", IPAddress(m_address).unparse().c_str(), multicastGroup.unparse().c_str());
    }
}

int IGMPGroupMember::joinGroupHandler(const String &conf, Element *e, void *thunk, ErrorHandler * errh) {
    in_addr* group = new in_addr();
    if(cp_va_kparse(conf, e, errh,
        "GROUP",    cpkM + cpkP,    cpIPAddress,        group,
        cpEnd) < 0) return -1;

    click_chatter("(HOST:%s)\tHandler call to join group %s", IPAddress(((IGMPGroupMember*)e)->m_address).unparse().c_str(), IPAddress(*group).unparse().c_str());
    ((IGMPGroupMember*)e)->joinGroup(*group);
    delete group;
    return 0;
}

int IGMPGroupMember::leaveGroupHandler(const String &conf, Element *e, void *thunk, ErrorHandler * errh) {
    in_addr* group = new in_addr();
    if(cp_va_kparse(conf, e, errh,
        "GROUP",    cpkM + cpkP,    cpIPAddress,        group,
        cpEnd) < 0) return -1;

    click_chatter("(HOST:%s)\tHandler call to leave group %s", IPAddress(((IGMPGroupMember*)e)->m_address).unparse().c_str(), IPAddress(*group).unparse().c_str());
    ((IGMPGroupMember*)e)->leaveGroup(*group);
    delete group;
    return 0;
}

void IGMPGroupMember::handleStateChangeTimer(Timer* timer, void* data) {
    StateChangeTimerData* sctd = (StateChangeTimerData*) data;
    assert(sctd);

    // If timer has been marked for deletion:
    if(sctd->me->m_stateChangeTimers.find(sctd->group) == sctd->me->m_stateChangeTimers.end()) {
        delete sctd;
    } else {
        click_chatter("(HOST:%s)\tSending State Change Report (Countdown: %d)", IPAddress(sctd->me->m_address).unparse().c_str(), sctd->retransCountdown);
        sctd->me->output(0).push(sctd->me->createIGMPPacket(IPAddress("244.0.0.22"), &sctd->report));

        // Last retransmission:
        if(sctd->retransCountdown == 1) {
            // Clean up: delete timer
            sctd->me->m_stateChangeTimers.erase(sctd->group);
            delete sctd;
            delete timer;
        } else {
            sctd->retransCountdown -= 1;
            timer->schedule_after_sec(IGMPGroupMember::randomTime(sctd->me->m_unsolicitedReportInterval));
        }
    }
}

void IGMPGroupMember::handleGeneralQueryTimer(Timer* timer, void* data) {
    click_chatter("(HOST:%s)\tResponding to General Query.", IPAddress(((IGMPGroupMember*)timer->element())->m_address).unparse().c_str());
    ((IGMPGroupMember*)timer->element())->sendAllGroupsReport();
}

void IGMPGroupMember::handleGroupSpecificTimer(Timer* timer, void* data) {
    click_chatter("(HOST:%s)\tResponding to Group Specific Query.", IPAddress(((IGMPGroupMember*)timer->element())->m_address).unparse().c_str());
    IPAddress addr = *((IPAddress*) data);
    ((IGMPGroupMember*)timer->element())->sendGroupReport(addr);
}

uint IGMPGroupMember::randomTime(uint rightBound) {
    // Can't use proper C++11 rand because of Click!
    return (rand() % (int)(rightBound + 1));
}

void IGMPGroupMember::scheduleReport(IPAddress group, MembershipReport mr) {
    StateChangeTimerData* sctd = new StateChangeTimerData(this, group, mr, m_robustnessVariable);
    Timer* stateChangeTimer = new Timer(&IGMPGroupMember::handleStateChangeTimer, (void*) sctd);
    stateChangeTimer->initialize(this);

    // Check for pending reports for this multicast group
    HashTable<IPAddress, Timer*>::iterator it;
    it = m_stateChangeTimers.find(group);
    // If report still in retransmission stage
    if(it != m_stateChangeTimers.end()) {
        Timer* oldTimer = it->second;
        // Mark timer for deletion by removing it from timer table
        m_stateChangeTimers.erase(group);
        // Timer data will be removed by the scheduled function, handleStateChangeTimer
        //      More info: we _only_ have access to the Timer's associated StateChangeTimerData
        //      data element when the timer is triggered, so to correctly free memory the
        //      StateChangeTimerData has to be deleted there
        oldTimer->schedule_after_sec(0);
        delete oldTimer;
    }

    m_stateChangeTimers.set(group, stateChangeTimer);
    stateChangeTimer->schedule_after_sec(0); // Send immediately
}


CLICK_ENDDECLS
ELEMENT_REQUIRES(MembershipQuery)
ELEMENT_REQUIRES(MembershipReport)
EXPORT_ELEMENT(IGMPGroupMember)
