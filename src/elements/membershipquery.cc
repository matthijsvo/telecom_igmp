#include <click/config.h>
#include <click/integers.hh>
#include "membershipquery.hh"

CLICK_DECLS

uint readCodefield(union codefield cf) {
	if(cf.integer < 128) return cf.integer;
	else return (cf.floatingpoint.mant | 0x10) << (cf.floatingpoint.expo +3);
}

MembershipQuery::MembershipQuery(in_addr group_address) : m_groupAddress(group_address) {}

MembershipQuery::MembershipQuery(void* packet) {
	membership_query_header* header = (membership_query_header*)packet;

	m_maxRespCode.integer 	= header->max_resp_code;
	m_checksum 				= header->checksum;
	m_groupAddress			= header->group_address;
	m_sFlag 				= header->mqhfields.fields.s;
	m_qrv 					= header->mqhfields.fields.qrv;
	m_qqic.integer 			= header->mqhfields.fields.qqic;

	header->checksum = 0; // Temporarily set to 0 to recalculate and check checksum
	if(m_checksum != click_in_cksum((const unsigned char *)packet, sizeof(membership_query_header))) {
		throw IGMPException("Invalid IGMP checksum!");
	}
	header->checksum = m_checksum;
}

MembershipQuery::~MembershipQuery() {}

void MembershipQuery::setMaxRespCode(uint8_t mrc) {
	union codefield cf;
	cf.integer = mrc;
	m_maxRespCode = cf;
}

void MembershipQuery::setMaxRespCode(uint mant, uint expo) {
	assert(mant <= 15);
	assert(expo <= 7);
	union codefield cf;
	cf.floatingpoint.i = 1;
	cf.floatingpoint.mant = mant;
	cf.floatingpoint.expo = expo;
	m_maxRespCode = cf;
}

void MembershipQuery::setSFlag(bool s) {
	m_sFlag = s;
}

void MembershipQuery::setQRV(uint qrv) {
	m_qrv = qrv;
}

void MembershipQuery::setQQIC(uint8_t mrc) {
	union codefield cf;
	cf.integer = mrc;
	m_qqic = cf;
}

void MembershipQuery::setQQIC(uint mant, uint expo) {
	assert(mant <= 15);
	assert(expo <= 7);
	union codefield cf;
	cf.floatingpoint.i = 1;
	cf.floatingpoint.mant = mant;
	cf.floatingpoint.expo = expo;
	m_qqic = cf;
}

uint MembershipQuery::getMaxRespCode() {
	return readCodefield(m_maxRespCode);
}

bool MembershipQuery::getSFlag() {
	return m_sFlag;
}

uint MembershipQuery::getQRV() {
	return m_qrv;
}

uint MembershipQuery::getQQIC() {
	return readCodefield(m_qqic);
}

bool MembershipQuery::isGeneralQuery() const {
	return (m_groupAddress == IPAddress("0.0.0.0"));
}

bool MembershipQuery::isGroupSpecificQuery() const {
	return (m_groupAddress != IPAddress("0.0.0.0"));
}

void* MembershipQuery::insertMessage(void* packet) {
	membership_query_header* header = (membership_query_header*)packet;
	header->type = 0x11;
	header->max_resp_code = m_maxRespCode.integer;
	header->checksum = 0; // Temporarily set to zero for later calculation of checksum
	header->group_address = m_groupAddress;
	header->mqhfields.fields.resv = 0;
	header->mqhfields.fields.s = m_sFlag;
	header->mqhfields.fields.qrv = m_qrv;
	header->mqhfields.fields.qqic = m_qqic.integer;
	header->number_of_sources = host_to_net_order(0);

	header->checksum = calculateChecksum(packet);
	packet = (void*)(header + 1);

	return packet;
}

uint MembershipQuery::getByteSize() {
	return sizeof(membership_query_header);
}

IPAddress MembershipQuery::getGroupAddress() {
	return IPAddress(m_groupAddress);
}

uint16_t MembershipQuery::calculateChecksum(void* packet) {
	return click_in_cksum((const unsigned char *)packet, getByteSize());
}

MembershipQuery MembershipQuery::GeneralQuery() {
	return MembershipQuery(IPAddress().in_addr());
}

MembershipQuery MembershipQuery::GroupSpecificQuery(in_addr targetMCAddress) {
	return MembershipQuery(targetMCAddress);
}

MembershipQuery* MembershipQuery::parseMessage(void* packet) {
	membership_query_header* header = (membership_query_header*)packet;

	MembershipQuery* mq = new MembershipQuery(header);

	uint16_t checksum = header->checksum;
	header->checksum = 0; // Temporarily set to 0 to recalculate and check checksum
	if(checksum != click_in_cksum((const unsigned char *)packet, sizeof(membership_query_header))) {
		throw IGMPException("Invalid IGMP checksum!");
		return NULL;
	}

	return mq;
}

void MembershipQuery::debugPrint() const {
	click_chatter("MEMBERSHIP QUERY:");
	click_chatter("| Max resp code: %d", m_maxRespCode.integer);
	click_chatter("| Group address: %s", IPAddress(m_groupAddress).unparse().c_str());
	click_chatter("| S Flag: \t%d", m_sFlag);
	click_chatter("| QRV: \t%d", m_qrv);
	click_chatter("| QQIC: \t%d", m_qqic.integer);
}

CLICK_ENDDECLS
ELEMENT_PROVIDES(MembershipQuery)
