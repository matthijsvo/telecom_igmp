#ifndef CLICK_ELEMENTMEMBERSHIPQUERY_HH
#define CLICK_ELEMENTMEMBERSHIPQUERY_HH

#include <click/element.hh>
#include <clicknet/ip.h>		//include the ip header structs
#include <click/integers.hh>	//contains the net_to_host_order and host_to_net_order as well as the used integer types
#include <stdio.h>
#include <assert.h>
#include <vector>
#include "igmpexception.hh"
#include "igmpmessage.hh"

CLICK_DECLS

/*
 *	A code field that could either be a simple integer or represent
 *	a number in floating point representation.
 *
 *	If lower than 128, this field can be read as any 8 bit unsigned integer
 *	If higher, it represents the following format:
 *
 *	 0 1 2 3 4 5 6 7
 *	+-+-+-+-+-+-+-+-+
 *	|1| exp | mant  |
 *	+-+-+-+-+-+-+-+-+
 *
 *	Use the 'readCodefield' function for easy access to the actual represented integer
 */
union codefield{
	struct {
		uint i : 1;
		uint expo : 3;
		uint mant : 4;
	} floatingpoint;
	uint8_t integer;
};
uint readCodefield(union codefield cf);

// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |  Type = 0x11  | Max Resp Code |           Checksum            |
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |                         Group Address                         |
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// | Resv  |S| QRV |     QQIC      |     Number of Sources (N)     |
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
struct membership_query_header {
	uint8_t 	type;
	uint8_t		max_resp_code;
	uint16_t	checksum;

	in_addr		group_address;

	union {
		struct __attribute__((packed)){
			uint 		resv 	: 4;
			uint 		s 		: 1;
			uint 		qrv 	: 3;
			uint8_t 	qqic;
		} fields;
		uint16_t bits;
	} 			mqhfields;
	uint16_t 	number_of_sources;
} __attribute__((packed));


/*
 * MembershipQuery
 *
 * Class for convenient creation, manipulation and parsing of Membership Query messages
 *
 * Sent by routers to neighbouring interfaces to query their reception state
 */
class MembershipQuery : public IGMPMessage {
public:
	MembershipQuery(in_addr group_address);

	/*
	 * Create MembershipQuery from raw packet data
	 * Expects pointer right at start of IGMP message data
	 */
	MembershipQuery(void* packet);

	~MembershipQuery();

	void setMaxRespCode(uint8_t mrc);
	void setMaxRespCode(uint mant, uint expo);
	void setSFlag(bool s);
	void setQRV(uint qrv);
	void setQQIC(uint8_t qqic);
	void setQQIC(uint mant, uint expo);

	uint getMaxRespCode();
	bool getSFlag();
	uint getQRV();
	uint getQQIC();
	IPAddress getGroupAddress();

	bool isGeneralQuery() const;
	bool isGroupSpecificQuery() const;

	/*
	 *	Inserts a raw data MembershipQuery into packet at pointer 'packet'
	 *	Assumes the necessary memory space for this Query has been reserved!
	 *	Returns pointer to RIGHT AFTER the membership query message that was just inserted
	 */
	void* insertMessage(void* packet);

	/*
	 *	Returns the size (in bytes) of this complete MembershipQuery in raw data form
	 *	This includes the header and any following source addresses
	 */
	uint getByteSize();

	/*
	 * General Query
	 * Sent by multicast router to learn the complete multicast reception state
	 * of neighbouring interfaces.
	 * Sent with IP destination 244.0.0.1
	 */
	static MembershipQuery GeneralQuery();
	/*
	 * Group-Specific Query
	 * Sent by multicast router to learn the reception state of a single multicast address
	 * from neighbouring interfaces
	 * Sent with IP destination the address of interest
	 */
	static MembershipQuery GroupSpecificQuery(in_addr targetMCAddress);

	static MembershipQuery* parseMessage(void* packet);

	void debugPrint() const;

private:
	/*
	 *	Returns the 16-bit one's complement of one's complement of the whole message
	 */
	uint16_t calculateChecksum(void* packet);

	// Max Resp Code
	// 	Represents maximum time allowed before sending responding report
	union codefield m_maxRespCode;

	// Checksum
	uint16_t 		m_checksum;

	// Group Address
	in_addr 		m_groupAddress;

	// S Flag
	//	If 1, surpresses normal timer updates from receiving Query
	bool 			m_sFlag;

	// Querier's Robustness Variable
	uint 			m_qrv : 3;

	// Querier's Query Interval Code
	//	Represents Querier's Query Interval
	union codefield m_qqic;

};

#endif
