#ifndef CLICK_IGMPELEMENT_HH
#define CLICK_IGMPELEMENT_HH

#include <click/element.hh>
#include <clicknet/ether.h>		//include the ethernet header structs 
#include <clicknet/ip.h>		//include the ip header structs
#include <clicknet/icmp.h>		//include the icmp header structs from click
#include <clicknet/udp.h>		//include the udp header structs from click
#include <click/timer.hh>
#include <stdio.h>
#include "igmpmessage.hh"
#include "membershipquery.hh"
#include "membershipreport.hh"
#include "misc.hh"

CLICK_DECLS

enum FilterMode{INCLUDE, EXCLUDE};

class IGMPElement : public Element{
public:
	IGMPElement();
	~IGMPElement();

	const char *class_name() const	{ return "IGMPElement"; }
	const char *port_count() const 	{ return "0"; }
	int configure(Vector<String> &conf, ErrorHandler *errh);

protected:
	in_addr m_address;

	Packet* createIGMPPacket(in_addr dst, IGMPMessage* msg);

	void parsePacket(Packet* packet);

	virtual void handleMembershipQuery(MembershipQuery mq) {};
	virtual void handleMembershipReport(MembershipReport mr) {};
	virtual void handleMulticastPacket(MembershipReport mr) {};

	/*
	 *	Checks if packet has valid IP header for an IGMP message
	 *	Returns pointer right after IP header (start of IGMP message)
	 */
	void* checkIGMPIPHeader(Packet* p);

	/*
	 * Returns IGMP type of message
	 * Assumes IP section of packet is constructed properly
	 */
	uint8_t getIGMPType(Packet* p);

};

CLICK_ENDDECLS
#endif
