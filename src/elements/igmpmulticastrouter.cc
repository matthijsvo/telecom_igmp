#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include "igmpmulticastrouter.hh"
#include "membershipquery.hh"

CLICK_DECLS

IGMPGroupState::IGMPGroupState() {
    filterMode = INCLUDE;
}

IGMPGroupState::IGMPGroupState(IPAddress multicast_ip, FilterMode mode) : 
    multicastIP(multicast_ip), filterMode(mode) {};

IGMPGroupState::~IGMPGroupState() {};

void IGMPGroupState::setFilterMode(FilterMode mode){
    //if(mode == EXCLUDE) 
        filterMode = mode;
}

void IGMPGroupState::setMulticastIP(IPAddress ip){
    multicastIP = ip;
}

FilterMode IGMPGroupState::getFilterMode(){
    return filterMode;
}

IPAddress IGMPGroupState::getMulticastIP(){
    return multicastIP;
}

bool IGMPGroupState::forwardPacket(){
    //Source specific forwarding rules without timers p.30
    if(filterMode == INCLUDE)
            return false;
    else //EXCLUDE  
        return true;
}

void IGMPGroupState::debugPrint() const{
    click_chatter("[ROUTER]\t \tGROUP STATE:");
    click_chatter("[ROUTER]\t \t| Multicast address: %s", multicastIP.unparse().c_str());
    if(filterMode == INCLUDE)
        click_chatter("[ROUTER]\t \t| Filter Mode: %s", "INCLUDE");
    else
        click_chatter("[ROUTER]\t \t| Filter Mode: %s", "EXCLUDE");
}

////////////////////////////////////////////////////////////////////////////////

IGMPMulticastRouter::IGMPMulticastRouter() {
    QRV = 2;
    QI = 125000;
    QRI = 10000;
    LMQI = 1000;
    LMQC = QRV;
    SQI = QI/4;
    SQC = QRV;
    GMI = (QRV * QI) + QRI;
    LMQT = LMQI * LMQC;
    generalQueryTimer = NULL;
}

IGMPMulticastRouter::~IGMPMulticastRouter() {}

int IGMPMulticastRouter::configure(Vector<String> &conf, ErrorHandler *errh){
    if(cp_va_kparse(conf, this, errh,
        "INTERFACE",      cpkM + cpkP,    cpIPAddress,        &interface,
        "QRV",            cpkP,           cpInteger,          &QRV,
        "QI",             cpkP,           cpInteger,          &QI,
        "QRI",            cpkP,           cpInteger,          &QRI,
        "LMQI",           cpkP,           cpInteger,          &LMQI,
        "LMQC",           cpkP,           cpInteger,          &LMQC,
        "SQI",            cpkP,           cpInteger,          &SQI,
        "SQC",            cpkP,           cpInteger,          &SQC,
        cpEnd) < 0) return -1;
    if(LMQC == 0)
        LMQC = QRV;
    if(SQI == 0)
        SQI = QI/4;
    if(SQC == 0)
        SQC = QRV;
    GMI = (QRV * QI) + QRI;
    LMQT = LMQI * LMQC;
    click_chatter("QRV: %d", QRV);
    click_chatter("QI: %d", QI);
    click_chatter("QRI: %d", QRI);
    click_chatter("LQMI: %d", LMQI);
    click_chatter("LMQC: %d", LMQC);
    click_chatter("SQI: %d", SQI);
    click_chatter("SQC: %d", SQC);
    sendGeneralQuery();
    --SQC;
    setGeneralQueryTimer();
    return 0;
}

void IGMPMulticastRouter::debugPrint() const{
    click_chatter("[ROUTER:%s]\t ROUTER STATE:", interface.unparse().c_str());
    for(HashTable<IPAddress,IGMPGroupState>::const_iterator it=groupStates.begin(); 
        it!=groupStates.end(); ++it){
        it->second.debugPrint();
    }
}

bool IGMPMulticastRouter::forwardPacket(IPAddress multicast){
    HashTable<IPAddress,IGMPGroupState>::iterator group = 
        groupStates.find(multicast);
    if(group != groupStates.end()){
        return group->second.forwardPacket();
    }
    return false;
}

//receive group specific query p.33
void IGMPMulticastRouter::handleMembershipQuery(MembershipQuery query) {
    click_chatter("[ROUTER:%s]\t Received query", interface.unparse().c_str());
    if(query.isGroupSpecificQuery() && !query.getSFlag()){
        setGroupTimer(query.getGroupAddress(), LMQT);
    }
}

void IGMPMulticastRouter::handleMembershipReport(MembershipReport report){
    click_chatter("[ROUTER:%s]\t Received report", interface.unparse().c_str());

    for(unsigned int i=0; i<report.getNumberOfGroupRecords(); i++) {
        GroupRecord groupRecord = report.getGroupRecord(i);
        IPAddress multicastIP = groupRecord.getMulticastAddress();
        int type = groupRecord.getType();
        HashTable<IPAddress, IGMPGroupState>::mapped_type* groupState = 
            groupStates.get_pointer(multicastIP);

        //Create group state if it does not exist for this multicast address
        //Current-State p.31
        if(type == GroupRecord::MODE_IS_INCLUDE){
            click_chatter("[ROUTER:%s]\t MODE_IS_INCLUDE", 
                interface.unparse().c_str());
            if(groupState == NULL){
                IGMPGroupState newGroupState = IGMPGroupState(multicastIP,INCLUDE);
                groupStates.set(multicastIP,newGroupState);
                return;
            }
            //do nothing, cannot change from exclude to include
        } 
        else if(type == GroupRecord::MODE_IS_EXCLUDE) {
            click_chatter("[ROUTER:%s]\t MODE_IS_EXCLUDE", 
                interface.unparse().c_str());
            if(groupState == NULL){
                IGMPGroupState newGroupState = IGMPGroupState(multicastIP,EXCLUDE);
                groupStates.set(multicastIP,newGroupState);
            }
            else groupState->setFilterMode(EXCLUDE);
            setGroupTimer(multicastIP, GMI);
        }
        //Filter-Mode-Change p.32-33
        else if(type == GroupRecord::CHANGE_TO_INCLUDE_MODE) {
            click_chatter("[ROUTER]\t CHANGE_TO_INCLUDE_MODE", 
                interface.unparse().c_str());
            if(groupState == NULL){
                IGMPGroupState newGroupState = IGMPGroupState(multicastIP,INCLUDE);
                groupStates.set(multicastIP,newGroupState);
            }
            sendGroupSpecificQuery(multicastIP);
            setGroupQueryTimer(multicastIP); //p.34
            setGroupTimer(multicastIP, LMQT); //lower timer to LQMT p.31
            //do nothing, cannot change from include to exclude
        }
        else if(type == GroupRecord::CHANGE_TO_EXCLUDE_MODE) {
            click_chatter("[ROUTER:%s]\t CHANGE_TO_EXCLUDE_MODE", 
                interface.unparse().c_str());
            if(groupState == NULL){
                IGMPGroupState newGroupState = IGMPGroupState(multicastIP,EXCLUDE);
                groupStates.set(multicastIP,newGroupState);
                return;
            }
            else groupState->setFilterMode(EXCLUDE);
            setGroupTimer(multicastIP, GMI);
        }
    }
    debugPrint();
}

void IGMPMulticastRouter::sendGroupSpecificQuery(IPAddress multicastIP) {
    MembershipQuery *query = new MembershipQuery(multicastIP.in_addr());
    query->setMaxRespCode(LMQI/100);
    query->setQQIC(QI/1000);
    query->setQRV(QRV);
    HashTable<IPAddress,Timer*>::iterator it = groupTimers.find(multicastIP);    
    if(it != groupTimers.end() && (it->second->expiry()-Timestamp::now()) > Timestamp((double) LMQT / 1000.0))
        query->setSFlag(true);
    Packet* packet = createIGMPPacket(multicastIP.in_addr(),query);
    output(0).push(packet);
    click_chatter("[ROUTER:%s]\t Sent a Group Specific Query to %s", 
        interface.unparse().c_str(), multicastIP.unparse().c_str());
}

void IGMPMulticastRouter::sendGeneralQuery(){
    MembershipQuery *query = new MembershipQuery(IPAddress().in_addr());
    query->setMaxRespCode(QRI/100);
    query->setQQIC(QI/1000);
    query->setQRV(QRV);
    Packet* packet = createIGMPPacket(IPAddress("224.0.0.1").in_addr(),query);
    output(0).push(packet);
    click_chatter("[ROUTER:%s]\t Sent a general query", 
        interface.unparse().c_str());
}

void IGMPMulticastRouter::push(int port, Packet *p){
    //Output 0 for IGMP messages or multicast packet that should be forwarded
    //Output 1 for non IGMP non multicast packets
    //Output 2 for multicast packets that should not be forwarded
    click_ip* ipHeader = const_cast<click_ip*>(p->ip_header());
    IPAddress dst = IPAddress(ipHeader->ip_dst);
    if(ipHeader->ip_p == 2) {
        click_chatter("[ROUTER:%s]\t Router received IGMP packet!", 
            interface.unparse().c_str());
        parsePacket(p);
        p->kill();
    }
    else if(dst.is_multicast()){
        if(forwardPacket(dst)){
            click_chatter("[ROUTER:%s]\t Forwarded packet from: %s", 
                interface.unparse().c_str(), dst.unparse().c_str());
            output(1).push(p);
        }
        else{
            click_chatter("[ROUTER:%s]\t Discarded packet from: %s", 
                interface.unparse().c_str(), dst.unparse().c_str());
            output(2).push(p);
        }
    }
}

void IGMPMulticastRouter::handleGroupTimerExpiry(Timer* timer, void* data){
    TimerData *timerdata = (TimerData*) data;
    assert(timerdata);
    timerdata->me->groupTimerExpiry(timerdata->groupIP);
}

void IGMPMulticastRouter::handleGeneralQueryTimerExpiry(Timer* timer, void* data){
    TimerData *timerdata = (TimerData*) data;
    assert(timerdata);
    timerdata->me->generalQueryTimerExpiry();
}

//p.32 maintain protocol robustness
void IGMPMulticastRouter::handleGroupQueryExpiry(Timer* timer, void* data){
    TimerData *timerdata = (TimerData*) data;
    assert(timerdata);
    if(timerdata->times > 0)
        timerdata->me->groupQueryTimerExpiry(timerdata->groupIP);
    --timerdata->times;
}

//delete or just switch to include? p.28
void IGMPMulticastRouter::groupTimerExpiry(IPAddress groupIP){
    /*
    HashTable<IPAddress, IGMPGroupState>::mapped_type* groupState = 
            groupStates.get_pointer(groupIP);
    groupState->setFilterMode(INCLUDE);
    */
    HashTable<IPAddress, IGMPGroupState>::iterator it = groupStates.find(groupIP);
    groupStates.erase(it);
}

void IGMPMulticastRouter::generalQueryTimerExpiry(){
    sendGeneralQuery();
    if(SQC > 0)
        --SQC;
    setGeneralQueryTimer();
}

void IGMPMulticastRouter::groupQueryTimerExpiry(IPAddress groupIP){
    sendGroupSpecificQuery(groupIP);
}

//p.27 group timers
void IGMPMulticastRouter::setGroupTimer(IPAddress groupIP, int interval){
    HashTable<IPAddress, Timer*>::iterator it = 
            groupTimers.find(groupIP);
    if(it != groupTimers.end() && it->second != NULL){
        it->second->unschedule();
        it->second->schedule_after_msec(interval);
        click_chatter("[ROUTER:%s]\t Set Group Timer for multicast IP: %s", 
        interface.unparse().c_str(), groupIP.unparse().c_str());        
        return;
    }
    TimerData* timerdata = new TimerData(this,groupIP);
    Timer* newTimer = new Timer(&IGMPMulticastRouter::handleGroupTimerExpiry,
            timerdata);
    groupTimers.set(groupIP,newTimer);
    newTimer->initialize(this);
    newTimer->schedule_after_msec(interval);

    click_chatter("[ROUTER:%s]\t Set Group Timer for multicast IP: %s", 
        interface.unparse().c_str(), groupIP.unparse().c_str());
}

//p.25 send general queries periodically
void IGMPMulticastRouter::setGeneralQueryTimer(){
    int interval = QI;
    if(SQC > 0)
        interval = SQI;
    if(generalQueryTimer != NULL){
        generalQueryTimer->schedule_after_msec(interval);
        return;
    }
    TimerData* timerdata = new TimerData(this);
    Timer* newTimer = new Timer(&IGMPMulticastRouter::handleGeneralQueryTimerExpiry,
            timerdata);
    generalQueryTimer = newTimer;
    generalQueryTimer->initialize(this);
    generalQueryTimer->schedule_after_msec(interval);
    click_chatter("[ROUTER:%s]\t Set General Query Timer", 
        interface.unparse().c_str());
}

//p.25 p.32 send group specific queries
void IGMPMulticastRouter::setGroupQueryTimer(IPAddress groupIP){
    HashTable<IPAddress, Timer*>::iterator it = 
            groupQueryTimers.find(groupIP);
    if(it != groupQueryTimers.end()){
        it->second->unschedule();
        it->second->schedule_after_msec(LMQI);
        click_chatter("[ROUTER:%s]\t Set Group Specific Query Timer for multicast IP: %s", 
            interface.unparse().c_str(), groupIP.unparse().c_str());        
        return;
    }
    TimerData* timerdata = new TimerData(this,groupIP,LMQC-1);
    Timer* newTimer = new Timer(&IGMPMulticastRouter::handleGroupQueryExpiry,
            timerdata);
    groupQueryTimers.set(groupIP,newTimer);
    newTimer->initialize(this);
    newTimer->schedule_after_msec(LMQI);

    click_chatter("[ROUTER:%s]\t Set Group Specific Query Timer for multicast IP: %s", 
        interface.unparse().c_str(), groupIP.unparse().c_str());
}

CLICK_ENDDECLS
EXPORT_ELEMENT(IGMPMulticastRouter)
