#ifndef CLICK_MISC_HH
#define CLICK_MISC_HH

#include <click/config.h>
#include <click/element.hh>
#include "igmpexception.hh"

CLICK_DECLS

struct ip_option{
	ip_option(uint8_t copied, uint8_t option_class, uint8_t option_number, uint8_t option_length, uint16_t option_value) :
			option_length(option_length), option_value(option_value)
	{
		option_type = (copied << 7) | (option_class << 6) | option_number;
	}
	uint8_t option_type;
	uint8_t option_length;
	uint16_t option_value;
};

CLICK_ENDDECLS

#endif