#include <click/config.h>
#include <click/args.hh>
#include <click/error.hh>
#include "igmpelement.hh"

CLICK_DECLS

IGMPElement::IGMPElement() {}

IGMPElement::~IGMPElement() {}

int IGMPElement::configure(Vector<String> &conf, ErrorHandler *errh) {}

Packet* IGMPElement::createIGMPPacket(in_addr dst, IGMPMessage* msg) {
	uint ethersize = sizeof(click_ether);
	uint ipsize = sizeof(click_ip) + sizeof(ip_option);	
	uint packetsize = ipsize + msg->getByteSize();

	WritablePacket* packet = Packet::make(ethersize, 0, packetsize, 0);

	if(!packet){
		click_chatter("Can't make MembershipQueryMessage packet for some reason!");
		return 0;
	}

	memset(packet->data(), 0, packet->length());

	click_ip* 	ipHeader 		= (click_ip*)packet->data();
	ip_option* 	routerAlert		= (ip_option*)(ipHeader + 1);
	void* 		endPtr 			= msg->insertMessage(routerAlert + 1);

	{
		{	// IP HEADER
			uint ipttl		= 1;
			uint ipprotocol = 2;

			ipHeader->ip_v		= 4;											// IP version
			ipHeader->ip_hl		= (sizeof(click_ip) + sizeof(ip_option)) >> 2;						// header length
			ipHeader->ip_len	= host_to_net_order((uint16_t)packetsize);		//total datagram length
			ipHeader->ip_id		= host_to_net_order((uint16_t)0);				//ip datagram id
			ipHeader->ip_off	= 0;											//ip datagram offset (fragmentation)
			ipHeader->ip_ttl	= ipttl;										//TTL
			ipHeader->ip_p		= ipprotocol;									//next protocol type
			ipHeader->ip_src	= m_address;										//source IP address
			ipHeader->ip_dst	= dst;			 								//destination IP address

			packet->set_dst_ip_anno(IPAddress(ipHeader->ip_dst));
			packet->set_ip_header(ipHeader, ipsize);
		}
		{	// IP ROUTER ALERT
			uint8_t copied_flag		= 1;	// \
			uint8_t option_class	= 0;	// |
			uint8_t option_number	= 20;	// + 
			uint8_t option_length	= 4;	// |
			uint16_t option_value	= 0;	// /

			*routerAlert = ip_option(1, 0, 20, 4, 0); //Basic configuration of RFC-2113, nothing else specified
		}
		ipHeader->ip_sum = click_in_cksum((const unsigned char *)ipHeader, ipsize);
		packet->timestamp_anno().assign_now();
	}

	return packet;
}

void IGMPElement::parsePacket(Packet* packet) {
	try {
		void* pptr = checkIGMPIPHeader(packet);
		uint8_t type = *((uint8_t*)(pptr)); // grab first 8 bits of IGMP message
		if(type == 0x11) { // MembershipQuery
			MembershipQuery query = MembershipQuery(pptr);
			handleMembershipQuery(query);
		} else if(type == 0x22) { // MembershipReport
			MembershipReport report = MembershipReport(pptr);
			handleMembershipReport(report);
		} else { // Unknown! Panic!
			throw IGMPException("Unknown IGMP Type, message probably corrupted.");
		}

	} catch (IGMPException e) {
		click_chatter("Failed to parse received packet:");
		click_chatter("%s", e.what());
	}
}

void* IGMPElement::checkIGMPIPHeader(Packet* p) {
	if(!p->has_network_header()) {
		throw IGMPException("Received message does not have an IP header!");
		return NULL;
	}

	click_ip* ipHeader = const_cast<click_ip*>(p->ip_header());

	if(!ipHeader->ip_p == 2) {
		throw IGMPException("Received message is not an IGMP message!");
		return NULL;
	}

	uint16_t checksum = ipHeader->ip_sum;
	ipHeader->ip_sum = 0; // Temporarily set to 0 to recalculate and check checksum
	
	if(!checksum == click_in_cksum((const unsigned char *)ipHeader, net_to_host_order(ipHeader->ip_len))) {
		throw IGMPException("Received message has incorrect IP checksum!");
		return NULL;
	}

	ipHeader->ip_sum = checksum;

	ip_option* routerAlert = (ip_option*)(ipHeader + 1);

	return (void*)(routerAlert + 1);
}

uint8_t IGMPElement::getIGMPType(Packet* p) {
	click_ip* 	ipHeader 		= (click_ip*)(p->ip_header());
	ip_option* 	routerAlert 	= (ip_option*)(ipHeader + 1);
	uint8_t*	igmpType		= (uint8_t*)(routerAlert + 1);
	return *igmpType;
}

CLICK_ENDDECLS
EXPORT_ELEMENT(IGMPElement)
