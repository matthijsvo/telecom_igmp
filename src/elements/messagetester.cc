#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include "messagetester.hh"

CLICK_DECLS
MessageTester::MessageTester() : m_sendTimer(this), m_switcher(0)
{}

MessageTester::~ MessageTester()
{}

int MessageTester::configure(Vector<String> &conf, ErrorHandler *errh) {
	if(cp_va_kparse(conf, this, errh,
		"SRC",		cpkM + cpkP, 	cpIPAddress, 		&m_address,
		"DST",		cpkM + cpkP, 	cpIPAddress, 		&m_dst,
		cpEnd) < 0) return -1;

	m_sendTimer.initialize(this);
	m_sendTimer.schedule_after_msec(250);
	return 0;
}

void MessageTester::run_timer(Timer* t) {
	if(m_switcher == 0) {
		output(0).push(createMembershipQueryPacket());
		parsePacket(createMembershipQueryPacket());
	}
	else {
		output(0).push(createMembershipReportPacket());
		parsePacket(createMembershipReportPacket());
	}

	m_switcher = (m_switcher + 1) % 2;

	t->schedule_after_msec(250);
}

Packet* MessageTester::createMembershipQueryPacket() {
	MembershipQuery mq = MembershipQuery(m_address);
	mq.setMaxRespCode(120);
	mq.setQQIC(110);
	click_chatter("Sending MembershipQuery!");
	return createIGMPPacket(m_dst, &mq);
}

Packet* MessageTester::createMembershipReportPacket() {
	MembershipReport mr = MembershipReport();
	GroupRecord gr1(GroupRecord::MODE_IS_INCLUDE, m_address);
	GroupRecord gr2(GroupRecord::CHANGE_TO_INCLUDE_MODE, m_dst);
	gr1.addSource(m_address);
	gr1.addSource(m_dst);
	mr.addGroupRecord(gr1);
	mr.addGroupRecord(gr2);
	click_chatter("Sending MembershipReport!");
	return createIGMPPacket(m_dst, &mr);
}

void MessageTester::handleMembershipQuery(MembershipQuery mq) {
	click_chatter("Ready to perform some magic on this Membership Query!");
	mq.debugPrint();
}

void MessageTester::handleMembershipReport(MembershipReport mr) {
	click_chatter("Ready to perform some magic on this Membership Report!");
	mr.debugPrint();
}

CLICK_ENDDECLS
ELEMENT_REQUIRES(IGMPElement)
ELEMENT_REQUIRES(IGMPMessage)
ELEMENT_REQUIRES(MembershipQuery)
ELEMENT_REQUIRES(MembershipReport)
EXPORT_ELEMENT(MessageTester)
