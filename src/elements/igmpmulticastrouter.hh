#ifndef CLICK_IGMPMULTICASTROUTER_HH
#define CLICK_IGMPMULTICASTROUTER_HH

#include <click/ipaddress.hh>
#include <click/hashtable.hh>
#include "membershipreport.hh"
#include "igmpelement.hh"

CLICK_DECLS

class IGMPGroupState {

    public:
        IGMPGroupState();
        IGMPGroupState(IPAddress multicastIP, FilterMode mode);
        ~IGMPGroupState();
        
        void setFilterMode(FilterMode mode);
        void setMulticastIP(IPAddress multicastIP);

        FilterMode getFilterMode();
        IPAddress getMulticastIP();

        bool forwardPacket();        
        void debugPrint() const;

    private:
        IPAddress multicastIP;
        FilterMode filterMode;
};

////////////////////////////////////////////////////////////////////////////////

class IGMPMulticastRouter : public IGMPElement {

	public:
		IGMPMulticastRouter();
		~IGMPMulticastRouter();
		
        const char *class_name() const  { return "IGMPMulticastRouter"; }
        const char *port_count() const  { return "2/3"; }
        const char *processing() const  { return PUSH; }
        int configure(Vector<String>&, ErrorHandler*);

        void debugPrint() const;
        void push(int, Packet *);

        static void handleGroupTimerExpiry(Timer* timer, void* data);
        static void handleGeneralQueryTimerExpiry(Timer* timer, void* data);
        static void handleGroupQueryExpiry(Timer* timer, void* data);

	private:

        int QRV;
        int QI;
        int QRI;
        int LMQI;
        int LMQC;
        int GMI;
        int LMQT;
        int SQI;
        int SQC;
        struct TimerData {
            IGMPMulticastRouter* me;
            IPAddress groupIP;
            int times;
            TimerData(IGMPMulticastRouter* m, IPAddress IP) : me(m), groupIP(IP) {}
            TimerData(IGMPMulticastRouter* m, IPAddress IP, int t) : me(m), groupIP(IP), times(t) {}
            TimerData(IGMPMulticastRouter* m) : me(m) {}
        };
        HashTable<IPAddress,Timer*> groupTimers;
        HashTable<IPAddress,Timer*> groupQueryTimers;
        Timer* generalQueryTimer;

        IPAddress interface;
        HashTable<IPAddress,IGMPGroupState> groupStates;
        bool forwardPacket(IPAddress multicastIP);
        void handleMembershipQuery(MembershipQuery query);
        void handleMembershipReport(MembershipReport report);
        
        void sendGroupSpecificQuery(IPAddress multicastIP);
        void sendGeneralQuery();
        
        void setGroupTimer(IPAddress groupIP, int interval);
        void setGeneralQueryTimer();
        void setGroupQueryTimer(IPAddress groupIP);
        void groupTimerExpiry(IPAddress groupIP);
        void generalQueryTimerExpiry();
        void groupQueryTimerExpiry(IPAddress groupIP);
};

CLICK_ENDDECLS
#endif
