#!/bin/bash

(echo "write client21/groupMember.join 224.0.2.2"; \
sleep 1; \
echo "write client22/groupMember.join 224.0.2.2"; \
sleep 1; \
echo "write client31/groupMember.join 224.0.2.2"; \
sleep 1; \
echo "write client32/groupMember.join 224.0.2.2"; \
sleep 12; \
echo "write client21/groupMember.leave 224.0.2.2"; \
echo "write client22/groupMember.leave 224.0.2.2"; \
sleep 1; \
echo "write client31/groupMember.leave 224.0.2.2"; \
echo "write client32/groupMember.leave 224.0.2.2"; \
sleep 2; \
echo "write client21/groupMember.join 224.0.2.2"; \
sleep 8; \
echo "write client21/groupMember.leave 224.0.2.2";\
) | telnet localhost $1
