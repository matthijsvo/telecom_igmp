#ifndef CLICK_MESSAGETESTER_HH
#define CLICK_MESSAGETESTER_HH

#include "igmpelement.hh"

CLICK_DECLS

class MessageTester : public IGMPElement { 
	public:
		MessageTester();
		~MessageTester();
		
		const char *class_name() const	{ return "MessageTester"; }
		const char *port_count() const	{ return "0-1/0-1"; }
		const char *processing() const	{ return PUSH; }
		int configure(Vector<String>&, ErrorHandler*);
		
		void run_timer(Timer* timer);

		Packet* createMembershipQueryPacket();
		Packet* createMembershipReportPacket();

		void handleMembershipQuery(MembershipQuery mq);
		void handleMembershipReport(MembershipReport mr);

	private:
		uint m_switcher;
		Timer m_sendTimer;
		in_addr m_dst;
};

CLICK_ENDDECLS
#endif