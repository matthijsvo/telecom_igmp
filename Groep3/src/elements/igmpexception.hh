#ifndef CLICK_ELEMENTIGMPEXCEPTION_HH
#define CLICK_ELEMENTIGMPEXCEPTION_HH

#include <exception>
#include <string>

CLICK_DECLS

class IGMPException: public std::exception
{
public:
    /** Constructor (C strings).
     *  @param message C-style string error message.
     *                 The string contents are copied upon construction.
     *                 Hence, responsibility for deleting the char* lies
     *                 with the caller. 
     */
    explicit IGMPException(const char* message):
     	m_msg(message)
    {}

    explicit IGMPException(const std::string& message):
      	m_msg(message)
    {}

    virtual ~IGMPException() throw ()
    {}

    /** Returns a pointer to the (constant) error description.
     *  @return A pointer to a const char*. The underlying memory
     *          is in posession of the Exception object. Callers must
     *          not attempt to free the memory.
     */
    virtual const char* what() const throw () {
    	return m_msg.c_str();
    }

protected:
    std::string m_msg;
};
CLICK_ENDDECLS
#endif