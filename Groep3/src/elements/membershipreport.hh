#ifndef CLICK_ELEMENTMEMBERSHIPREPORT_HH
#define CLICK_ELEMENTMEMBERSHIPREPORT_HH

#include <click/element.hh>
#include <clicknet/ip.h>		//include the ip header structs
#include <click/integers.hh>	//contains the net_to_host_order and host_to_net_order as well as the used integer types
#include <stdio.h>
#include <vector>
#include "igmpexception.hh"
#include "igmpmessage.hh"

CLICK_DECLS

// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |  Record Type  |  Aux Data Len |     Number of Sources (N)     |
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |                       Multicast Address                       |
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
struct group_record_header {
	uint8_t 	record_type;
	uint8_t 	aux_data_len;
	uint16_t 	number_of_sources;
	in_addr		multicast_address;
	group_record_header() : aux_data_len(0), number_of_sources(0) {}
} __attribute__((packed));

/*
 * GroupRecord
 *
 * Class for convenient creation, manipulation and parsing of Group Records
 *
 * Contains information about the sender's membership in a single multicast group
 * on the interface from which a MembershipReport is sent
 */
class GroupRecord {
public:
	// MODE_IS_INCLUDE
	// Indicates that the interface has a filter mode of INCLUDE for the specified multicast
	// address. The Source Address [i] fields in this Group Record contain the interface’s source 
	// list for the specified multicast address, if it is non-empty.
	const static int MODE_IS_INCLUDE = 1;
	// MODE_IS_EXCLUDE
	// Indicates that the interface has a filter mode of EXCLUDE for the specified multicast
	// address. The Source Address [i] fields in this Group Record contain the interface’s source list 
	// for the specified multicast address, if it is non-empty.
	const static int MODE_IS_EXCLUDE = 2;
	// CHANGE_TO_INCLUDE_MODE 
	// Indicates that the interface has changed to INCLUDE filter mode for the specified
	// multicast address. The Source Address [i] fields in this Group Record contain the interface’s 
	// new source list for the specified multicast address, if it is non-empty.
	const static int CHANGE_TO_INCLUDE_MODE = 3;
	// CHANGE_TO_EXCLUDE_MODE 
	// Indicates that the interface has changed to EXCLUDE filter mode for the specified
	// multicast address. The Source Address [i] fields in this Group Record contain the interface’s 
	// new source list for the specified multicast address, if it is non-empty.
	const static int CHANGE_TO_EXCLUDE_MODE = 4;

	GroupRecord(int recordType, in_addr multicastAddress);
	/*
	 * Parses GroupRecord from raw packet data
	 * Expects pointer right at start of the group record
	 * IMPORTANT: the given pointer is UPDATED to the position RIGHT AFTER the parsed group record
	 */
	GroupRecord(void*& packet);
	~GroupRecord();
	in_addr getMulticastAddress();
	uint16_t getNumberOfSources();
	in_addr getSource(int i);
    uint8_t getType(); 

	void addSource(in_addr ipv4_source_address);
	void addSources(std::vector<in_addr> ipv4_source_addresses);

	/*
	 *	Inserts a raw data Group Record into packet at pointer 'packet'
	 *	Assumes the necessary memory space has been reserved!
	 *	Returns pointer to RIGHT AFTER the Group Record that was just inserted
	 */
	void* insertMessage(void* packet);

	/*
	 *	Returns the size (in bytes) of this complete GroupRecord in raw data form
	 *	This includes the header and any source addresses
	 */
	uint getByteSize();

	void debugPrint() const;

private:
	uint8_t m_recordType;
	in_addr m_multicastAddress;
	std::vector<in_addr> m_sourceAddresses;
};


// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |  Type = 0x22  |    Reserved   |           Checksum            |
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// |           Reserved            |  Number of Group Records (M)  |
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
struct membership_report_header {
	uint8_t 	type;
	uint8_t		reserved;
	uint16_t	checksum;
	uint16_t	reserved2;
	uint16_t	number_of_group_records;
	membership_report_header() : type(0x22), reserved(0), checksum(0), reserved2(0), number_of_group_records(0) {}
} __attribute__((packed));

/*
 * MembershipReport
 *
 * Class for convenient creation, manipulation and parsing of Membership Report messages
 *
 * Sent by IP systems to report to neighbouring routers:
 * 		- The current multicast reception state of their interfaces
 *		- Changes in the multicast reception state of their interfaces
 *
 * Sent with IP destination address 244.0.0.22
 */
class MembershipReport : public IGMPMessage {
public:
	MembershipReport();
	/*
	 * Parses MembershipReport from raw packet data
	 * Expects pointer right at start of IGMP message
	 */
	MembershipReport(void* packet);
	~MembershipReport();

	int getNumberOfGroupRecords();
	GroupRecord getGroupRecord(int i);

	void addGroupRecord(GroupRecord groupRecord);

	/*
	 *	Inserts a raw data Membership Report into packet at pointer 'packet'
	 *	Assumes the necessary memory space for this Report has been reserved!
	 *	Returns pointer to RIGHT AFTER the membership report message that was just inserted
	 */
	void* insertMessage(void* packet);

	/*
	 *	Returns the size (in bytes) of this complete MembershipReport in raw data form
	 *	This includes the header and any following Group Records
	 */
	uint getByteSize();

	static MembershipReport* parseMessage(void* packet);

	void debugPrint() const;
private:
	/*
	 *	Returns the 16-bit one's complement of one's complement of the whole message
	 */
	uint16_t getChecksum(void* packet);

	// Group Record [i]
	std::vector<GroupRecord> m_groupRecords;
};

#endif
