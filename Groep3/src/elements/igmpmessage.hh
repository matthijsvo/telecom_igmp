#ifndef CLICK_IGMPMESSAGE_HH
#define CLICK_IGMPMESSAGE_HH

#include "misc.hh"

CLICK_DECLS

class IGMPMessage {
public:
	virtual void* insertMessage(void* packet) = 0;
	virtual uint getByteSize() = 0;
};

CLICK_ENDDECLS
#endif