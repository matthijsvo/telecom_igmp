#ifndef CLICK_IGMPGROUPMEMBER_HH
#define CLICK_IGMPGROUPMEMBER_HH
#include <click/element.hh>
#include <click/packet.hh>
#include <click/timer.hh>
#include <click/hashtable.hh>
#include <map>
#include "igmpelement.hh"

class IGMPGroupMember : public IGMPElement{
public:
    IGMPGroupMember();
    ~IGMPGroupMember() {}

    const char *class_name() const  { return "IGMPGroupMember"; }
    const char *port_count() const  { return "1/2"; }
    const char *processing() const  { return PUSH; }
    int configure(Vector<String> &conf, ErrorHandler *errh);

    void push(int port, Packet* p);

    void handleMembershipQuery(MembershipQuery mq);
    void handleMembershipReport(MembershipReport mr);
    void handleMulticastPacket(Packet* p);

    void sendGroupReport(IPAddress addr);
    void sendAllGroupsReport();

    void joinGroup(IPAddress multicastGroup);
    void leaveGroup(IPAddress multicastGroup);
    static int joinGroupHandler(const String &conf, Element *e, void *thunk, ErrorHandler * errh);
    static int leaveGroupHandler(const String &conf, Element *e, void *thunk, ErrorHandler * errh);

    static void handleStateChangeTimer(Timer* timer, void* data);
    static void handleGeneralQueryTimer(Timer* timer, void* data);
    static void handleGroupSpecificTimer(Timer* timer, void* data);

    void add_handlers();

private:
    static uint randomTime(uint rightBound);
    void scheduleReport(IPAddress group, MembershipReport mr);


private:
    struct StateChangeTimerData {
        IGMPGroupMember* me;
        IPAddress group;
        MembershipReport report;
        uint retransCountdown;
        StateChangeTimerData(IGMPGroupMember* m, IPAddress multicastGroup, MembershipReport mr, uint robustnessVariable) :
            me(m), group(multicastGroup), report(mr), retransCountdown(robustnessVariable) {}
    };

private:
    uint m_robustnessVariable;
    uint m_unsolicitedReportInterval;

    HashTable<IPAddress, FilterMode> m_groupStates;

    HashTable<IPAddress, Timer*>    m_stateChangeTimers;
    Timer                           m_generalQueryTimer;
    HashTable<IPAddress, Timer*>    m_groupSpecificTimers;
};

#endif
