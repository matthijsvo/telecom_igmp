#include <click/config.h>
#include "membershipreport.hh"

CLICK_DECLS

GroupRecord::GroupRecord(int recordType, in_addr multicastAddress) :
		m_recordType(recordType), m_multicastAddress(multicastAddress)
		{}

GroupRecord::GroupRecord(void*& packet) {
	group_record_header* header = (group_record_header*)packet;
	m_recordType 		= net_to_host_order(header->record_type);
	m_multicastAddress 	= header->multicast_address;

	uint16_t numSources = net_to_host_order((uint16_t) header->number_of_sources);
	if(numSources > 0) {
		in_addr* source_address = (in_addr*)(header + 1);
		m_sourceAddresses.push_back(*source_address);
		for(int i = 1; i < numSources; ++i) {
			++source_address; // move the pointer 1 in_addr (in bytes) ahead
			m_sourceAddresses.push_back(*source_address);
		}

		packet = (void*)(source_address + 1); // move packet pointer to right after the data we just inserted
	}
	else packet = (void*)(header + 1);
}

GroupRecord::~GroupRecord() {}

in_addr GroupRecord::getMulticastAddress() {
	return m_multicastAddress;
}

uint16_t GroupRecord::getNumberOfSources() {
	return m_sourceAddresses.size();
}

in_addr GroupRecord::getSource(int i) {
	return m_sourceAddresses.at(i);
}

uint8_t GroupRecord::getType() {
    return m_recordType;
}

void GroupRecord::addSource(in_addr ipv4_source_address) {
	m_sourceAddresses.push_back(ipv4_source_address);
}

void GroupRecord::addSources(std::vector<in_addr> ipv4_source_addresses) {
	m_sourceAddresses.insert(m_sourceAddresses.end(), ipv4_source_addresses.begin(), ipv4_source_addresses.end());
}

void* GroupRecord::insertMessage(void* packet) {
	group_record_header* header = (group_record_header*)packet;
	header->record_type 		= host_to_net_order(m_recordType);
	header->number_of_sources 	= host_to_net_order((uint16_t)getNumberOfSources());
	header->multicast_address 	= m_multicastAddress;

	if(getNumberOfSources() > 0) {
		in_addr* source_address = (in_addr*)(header + 1);
		*source_address = m_sourceAddresses.at(0);
		for(int i = 1; i < getNumberOfSources(); ++i) {
			++source_address; // move the pointer 1 in_addr (in bytes) ahead
			*source_address = m_sourceAddresses.at(i);
		}

		packet = (void*)(source_address + 1); // move packet pointer to right after the data we just inserted
	}
	else packet = (void*)(header + 1);
	return packet;
}

uint GroupRecord::getByteSize() {
	return sizeof(group_record_header) + m_sourceAddresses.size()*sizeof(in_addr);
}

void GroupRecord::debugPrint() const {
	click_chatter("\tGROUP RECORD:");
	click_chatter("\t| Record type: \t%d", m_recordType);
	click_chatter("\t| Multicast address: %s", IPAddress(m_multicastAddress).unparse().c_str());
	for(int i = 0; i < m_sourceAddresses.size(); ++i) {
		click_chatter("\t\t| Source: %s",  IPAddress(m_sourceAddresses.at(i)).unparse().c_str());
	}
}



MembershipReport::MembershipReport()
{}

MembershipReport::MembershipReport(void* packet) {
	membership_report_header* header = (membership_report_header*) packet;
	uint16_t numGroupRecords = net_to_host_order((uint16_t) header->number_of_group_records);
	uint checksum = header->checksum;

	if(numGroupRecords > 0) {
		void* groupRecordPtr = (void*)(header + 1);
		m_groupRecords.push_back(GroupRecord(groupRecordPtr));
		for(int i = 1; i < numGroupRecords; ++i) {
			m_groupRecords.push_back(GroupRecord(groupRecordPtr));
		}

		header->checksum = 0; // Temporarily set to 0 to recalculate and check checksum
		if(checksum != click_in_cksum((const unsigned char *)packet, getByteSize())) {
			throw IGMPException("Invalid IGMP checksum!");
		}
		header->checksum = checksum;

		packet = groupRecordPtr; // We move the final pointer past the last data we inserted
	}
	else {
		header->checksum = 0; // Temporarily set to 0 to recalculate and check checksum
		if(checksum != click_in_cksum((const unsigned char *)packet, getByteSize())) {
			throw IGMPException("Invalid IGMP checksum!");
		}
		header->checksum = checksum;

		packet = (void*)(header + 1); // We move the final pointer past the last data we inserted
	}
}

MembershipReport::~MembershipReport()
{}

int MembershipReport::getNumberOfGroupRecords() {
	return m_groupRecords.size();
}

GroupRecord MembershipReport::getGroupRecord(int i) {
	return m_groupRecords.at(i);
}

void MembershipReport::addGroupRecord(GroupRecord groupRecord) {
	m_groupRecords.push_back(groupRecord);
}

void* MembershipReport::insertMessage(void* packet) {
	membership_report_header* header = (membership_report_header*)packet;
	header->type 					= 0x22;
	header->reserved 				= 0;
	header->reserved2 				= 0;
	header->checksum 				= 0; // Temporarily set to zero for later calculation of checksum
	header->number_of_group_records = host_to_net_order((uint16_t) getNumberOfGroupRecords());

	if(getNumberOfGroupRecords() > 0) {
		GroupRecord* groupRecord = (GroupRecord*)(header + 1);
		groupRecord = (GroupRecord*)m_groupRecords.at(0).insertMessage(groupRecord); // we receive the next position for the pointer from insertMessage
		for(int i = 1; i < getNumberOfGroupRecords(); ++i) {
			groupRecord = (GroupRecord*)m_groupRecords.at(i).insertMessage(groupRecord);
		}

		header->checksum = getChecksum(packet);
		packet = (void*)(groupRecord + 1); // We move the final pointer past the last data we inserted
	}
	else {
		header->checksum = getChecksum(packet);
		packet = (void*)(header + 1); // We move the final pointer past the last data we inserted
	}

	return packet;
}

uint MembershipReport::getByteSize() {
	uint groupRecordSize = 0;
	for(int i = 0; i < m_groupRecords.size(); ++i) {
		groupRecordSize += m_groupRecords.at(i).getByteSize();
	}
	return sizeof(membership_report_header) + groupRecordSize;
}

uint16_t MembershipReport::getChecksum(void* packet) {
	return click_in_cksum((const unsigned char *)packet, getByteSize());
}

void MembershipReport::debugPrint() const {
	click_chatter("MEMBERSHIP REPORT:");
	for(int i = 0; i < m_groupRecords.size(); ++i) {
		m_groupRecords.at(i).debugPrint();
	}
}

CLICK_ENDDECLS
ELEMENT_PROVIDES(MembershipReport)
